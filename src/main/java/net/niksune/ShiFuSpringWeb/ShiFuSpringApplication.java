package net.niksune.ShiFuSpringWeb;

import net.niksune.ShiFuSpringCore.services.Arene;
import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ShiFuSpringApplication {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(ShiFuSpringApplication.class, args);

        ShiFuMiste rocky = new ShiFuMiste("The Rock", 80, 15, 5);
        ShiFuMiste edouard = new ShiFuMiste("Edouard aux Mains d'Argent", 15, 5, 80);
        ShiFuMiste konan = new ShiFuMiste("Konan de Ame", 5, 80, 15);

        for (int i = 0; i < 5; i++) {
            System.out.println(Arene.combat(rocky, edouard, 3));
            System.out.println(Arene.combat(edouard, konan, 3));
            System.out.println(Arene.combat(konan, rocky, 3));
        }

    }


}
