package net.niksune.ShiFuSpringWeb.services;

import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class BDDService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ShiFuMiste shifumisteFromID(int id){

        System.out.println("passage par service");

        return jdbcTemplate.queryForObject("SELECT * FROM shifumiste WHERE ID = ?",
                (rs, rowNum) -> new ShiFuMiste(rs.getInt("ID"), rs.getString("Nom"),
                        rs.getShort("Pierre"), rs.getShort("Feuille"), rs.getShort("Ciseau")),
                id);
    }

}


