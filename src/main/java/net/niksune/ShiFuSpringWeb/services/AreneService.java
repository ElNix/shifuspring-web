package net.niksune.ShiFuSpringWeb.services;

import net.niksune.ShiFuSpringCore.beans.Victoire;
import net.niksune.ShiFuSpringCore.services.Arene;
import net.niksune.ShiFuSpringWeb.forms.CombatDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AreneService {

    @Autowired
    private BDDService bddService;

    public Victoire combatByDefinition(CombatDefinition cd){
        return Arene.combat(bddService.shifumisteFromID(cd.getIdShifumiste1()),
                bddService.shifumisteFromID(cd.getIdShifumiste2()),
                cd.getNbManchesGagnantes());
    }

}
