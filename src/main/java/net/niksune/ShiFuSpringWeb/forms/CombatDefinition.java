package net.niksune.ShiFuSpringWeb.forms;

public class CombatDefinition {

    private int idShifumiste1;
    private int idShifumiste2;
    private int nbManchesGagnantes;

    @Override
    public String toString() {
        return "CombatDefinition{" +
                "idShifumiste1=" + idShifumiste1 +
                ", idShifumiste2=" + idShifumiste2 +
                ", nbManchesGagnantes=" + nbManchesGagnantes +
                '}';
    }

    public int getIdShifumiste1() {
        return idShifumiste1;
    }

    public void setIdShifumiste1(int idShifumiste1) {
        this.idShifumiste1 = idShifumiste1;
    }

    public int getIdShifumiste2() {
        return idShifumiste2;
    }

    public void setIdShifumiste2(int idShifumiste2) {
        this.idShifumiste2 = idShifumiste2;
    }

    public int getNbManchesGagnantes() {
        return nbManchesGagnantes;
    }

    public void setNbManchesGagnantes(int nbManchesGagnantes) {
        this.nbManchesGagnantes = nbManchesGagnantes;
    }
}

