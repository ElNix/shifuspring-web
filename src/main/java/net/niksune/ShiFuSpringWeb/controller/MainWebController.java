package net.niksune.ShiFuSpringWeb.controller;

import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import net.niksune.ShiFuSpringCore.beans.Victoire;
import net.niksune.ShiFuSpringWeb.forms.CombatDefinition;
import net.niksune.ShiFuSpringWeb.services.AreneService;
import net.niksune.ShiFuSpringWeb.services.BDDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/ShiFuSpring")
public class MainWebController {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private BDDService bddService;
    @Autowired
    private AreneService areneService;

    @RequestMapping("/demoPrincipale")
    public String demoPrincipale(){
        System.out.println("Lancement de la démo principale en controleur :");
        return "demoPrincipale";
    }

    @GetMapping
    public List<ShiFuMiste> getAllShifumist(){
        System.out.println("Récupérer tous les shifumists");
        return jdbcTemplate.query("SELECT * FROM shifumiste",
                (rs, rowNum) -> new ShiFuMiste(rs.getInt("ID"), rs.getString("Nom"),
                        rs.getShort("Pierre"), rs.getShort("Feuille"), rs.getShort("Ciseau")));
    }

    @GetMapping("/{id}")
    public ShiFuMiste getShifumistById(@PathVariable("id") String id){
        System.out.println("Récupérer le shifumist ID : "+id);
        return bddService.shifumisteFromID(Integer.parseInt(id));
    }

    @PostMapping
    public int postShifumist(@RequestBody ShiFuMiste shifumiste){
        System.out.println("Ajout de : "+shifumiste);
        // Return 1 if OK
        return jdbcTemplate.update(
                "INSERT INTO shifumiste (Nom,Pierre,Feuille,Ciseau) VALUES (?,?,?,?)",
                shifumiste.getNom(),
                shifumiste.getPierre(),
                shifumiste.getFeuille(),
                shifumiste.getCiseau());
    }

    @PostMapping("/combat")
    public Victoire combat(@RequestBody CombatDefinition cd) {

        System.out.println("Combat avec cette définition : "+cd);
        return areneService.combatByDefinition(cd);

    }




}

